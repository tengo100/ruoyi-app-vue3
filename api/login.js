import request from '@/utils/request'

// 登录方法
export function login(username, password, code, uuid,lngorganizeid) {
  const data = {
    username,
    password,
    code,
    uuid,
	lngorganizeid
  }
  return request({
    'url': '/login',
    headers: {
      isToken: false
    },
    'method': 'post',
    'data': data
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    'url': '/getInfo',
    'method': 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    'url': '/logout',
    'method': 'post'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    'url': '/captchaImage',
    headers: {
      isToken: false
    },
    method: 'get',
    timeout: 20000
  })
}

// 根据用户名获取组织机构树
export function getOrganizeByUser(data) {
    return request({
        url: '/getOrganizeByUser',
		headers: {
		  isToken: false
		},
        method: 'post',
        data
    })
}