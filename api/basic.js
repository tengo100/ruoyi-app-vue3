import request from '@/utils/request'

// 查询详细
export function getBilltemplate(lngbilltemplateid) {
    return request({
        url: '/basic/billtemplate/' + lngbilltemplateid,
        method: 'get'
    })
}

// 查询数据项启用详细
export function getReceipttype(lngreceipttypeid) {
    return request({
        url: '/basic/receipttype/' + lngreceipttypeid,
        method: 'get'
    })
}

// 获取单据号
export function getStrbillno(data){
    return request({
        url: '/basic/receiptrule/getStrbillno',
        method: 'POST',
        data
    })
}

// 获取动态公共数据接口
export function getStandardBasicdataTree(data){
  return request({
    url: "/basic/basicdata/getStandardBasicdataTree",
    method: 'post',
    data: data
  })
}

// 数据来源下拉框
export function listDatasource() {
    return request({
        url: '/basic/common/getDatasource',
        method: 'get'
    })
}

// 获取模板动态列
export function getTemplateData(lngreceipttypeid){
    return request({
        url: '/basic/billtemplate/getTemplateData/'+lngreceipttypeid,
        method: 'get'
    })
}

export function getStadardApi(data){
  return request({
    url: "/basic/common/getStadardApi",
    method: 'post',
    data: data
  })
}