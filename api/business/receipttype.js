import request from '@/utils/request'

// 新增编辑预置项
export function addOrUpdate(data) {
    return request({
        url: '/basic/receipttype',
        method: 'post',
        data
    })
}
// 查询数据项启用详细
export function getReceipttype(lngreceipttypeid) {
    return request({
        url: '/basic/receipttype/' + lngreceipttypeid,
        method: 'get'
    })
}
// 查询所有
export function getAll() {
    return request({
        url: '/basic/receipttype/getAll',
        method: 'get'
    })
}
