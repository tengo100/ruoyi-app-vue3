import request from '@/utils/request'

// 新增编辑预置项
export function addOrUpdate(data) {
    return request({
        url: '/basic/receipttyperelation',
        method: 'post',
        data
    })
}
// 查询数据项启用详细
export function getReceipttypeRelation(lngreceipttyperelationid) {
    return request({
        url: '/basic/receipttyperelation/' + lngreceipttyperelationid,
        method: 'get'
    })
}
