import request from '@/utils/request'


// 查询公告列表
export function listBusinessdata(query) {
    return request({
        url: '/basic/businessdata/list',
        method: 'get',
        params: query
    })
}

// 查询公告详细
export function getBusinessdata(lngreceipttypeid,id) {
    return request({
        url: '/basic/businessdata/' + lngreceipttypeid+'/'+id,
        method: 'get'
    })
}

// 新增公告
export function addBusinessdata(data) {
    return request({
        url: '/basic/businessdata',
        method: 'post',
        data: data
    })
}

// 修改公告
export function updateBusinessdata(data) {
    return request({
        url: '/basic/businessdata',
        method: 'put',
        data: data
    })
}

// 删除公告
export function delBusinessdata(lngreceipttypeid,lngmainid) {
    return request({
        url: `/basic/businessdata/${lngreceipttypeid}/${lngmainid}`,
        method: 'delete'
    })
}
// 根据关系获取
export function findByRelationId(lngreceipttyperelationid,id) {
    return request({
        url: '/basic/businessdata/findRelationdata/'+lngreceipttyperelationid+'/' + id,
        method: 'get'
    })
}

// 获取导入细表的模版下载
export function getImportTemplate(lngreceipttypeid,lngbilltemplateid) {
    return request({
        url:`/basic/businessdata/download/${lngreceipttypeid}/${lngbilltemplateid}`,
        method: 'get',
        responseType: 'blob'
    })
}
// 终止
export function closeEvent(lngreceipttypeid, id){
    return request({
        url: '/basic/businessdata/close/'+lngreceipttypeid+'/' + id,
        method: 'get'
    })
}
// 取消终止
export function cancelCloseEvent(lngreceipttypeid, id){
    return request({
        url: '/basic/businessdata/cancelClose/'+lngreceipttypeid+'/' + id,
        method: 'get'
    })
}
