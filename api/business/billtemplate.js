import request from '@/utils/request'

// 获取单据类型下拉
export function getReceiptTypeList() {
    return request({
        url: '/basic/receipttype/getAll',
        method: 'get'
    })
}

// 查询详细
export function getBilltemplate(lngbilltemplateid) {
    return request({
        url: '/basic/billtemplate/' + lngbilltemplateid,
        method: 'get'
    })
}
// 新增
export function addBilltemplate(data) {
    return request({
        url: '/basic/billtemplate',
        method: 'post',
        data: data
    })
}

// 修改
export function updateBilltemplate(data) {
    return request({
        url: '/basic/billtemplate',
        method: 'put',
        data: data
    })
}

// 删除
export function delParam(lngparamid) {
    return request({
        url: '/basic/billtemplate/' + lngparamid,
        method: 'delete'
    })
}
// 根据单据类型获取单据模板
export function getBilltemplateByType(lngreceipttypeid){
    return request({
        url: '/basic/billtemplate/getBilltemplate/'+lngreceipttypeid,
        method: 'get'
    })
}
export function getTemplateData(lngreceipttypeid){
    return request({
        url: '/basic/billtemplate/getTemplateData/'+lngreceipttypeid,
        method: 'get'
    })
}

// 删除明细行检查
export function deleteRowCheck(data){
    return request({
        url: '/basic/billtemplate/deleteRowCheck',
        method: 'post',
        data: data
    })
}
